(setq gc-cons-threshold 8000000
      gc-cons-percentage 0.6)

;; Prevent the glimpse of un-styled Emacs by disabling these UI elements early.
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)
;(push '(fullscreen . maximized) default-frame-alist)

(setq default-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

(when (featurep 'pgtk-win)
  (setq pgtk-use-im-context-on-new-connection nil)
  (setq pgtk-use-im-context nil))

(add-hook
 'emacs-startup-hook
 (lambda (&rest _)
   (setq gc-cons-percentage 0.1)
   (setq file-name-handler-alist default-file-name-handler-alist)

   ;; delete no longer necessary startup variable
   (makunbound 'default-file-name-handler-alist)))
