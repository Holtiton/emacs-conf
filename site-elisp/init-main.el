;; [[file:../init.org::*Preamble][Preamble:1]]
;; -*- lexical-binding: t; -*-
;; Preamble:1 ends here

;; [[file:../init.org::*Package management][Package management:1]]
(require 'package)
(add-to-list 'package-archives
	         '("melpa" . "https://melpa.org/packages/") t)

(setopt use-package-enable-imenu-support t)
(require 'use-package)
;; Package management:1 ends here

;; [[file:../init.org::*Customization][Customization:1]]
(setq custom-file (concat user-emacs-directory "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))
;; Customization:1 ends here

;; [[file:../init.org::*Global keybinds][Global keybinds:1]]
(substitute-key-definition 'dabbrev-expand 'hippie-expand (current-global-map))
(substitute-key-definition 'list-buffers 'ibuffer (current-global-map))
(substitute-key-definition 'zap-to-char 'zap-up-to-char (current-global-map))

(keymap-global-set "C-x C-r" 'recentf)
(keymap-global-set "C-x v f" 'vc-git-grep)

(keymap-global-set "C-c a" 'org-agenda)
(keymap-global-set "C-c c" 'org-capture)
(keymap-global-set "C-c w" 'whitespace-mode)

(keymap-global-set "C-x C-p" 'previous-buffer)
(keymap-global-set "C-x C-n" 'next-buffer)

(keymap-global-set "C-x ." 'scratch-buffer)
(keymap-global-set "C-x ," 'eshell)

;; both these suspend the frame, this is a completely open binding
(keymap-global-unset "C-z")
(keymap-global-unset "C-x C-z")

;; don't need the menu on this binding
(keymap-global-unset "M-`")
;; Global keybinds:1 ends here

;; [[file:../init.org::*Global keybinds][Global keybinds:2]]
(defun repeated-prefix-help-command ()
  (interactive)
  (when-let* ((keys (this-command-keys-vector))
              (prefix (seq-take keys (1- (length keys))))
              (orig-keymap (key-binding prefix 'accept-default))
              (keymap (copy-keymap orig-keymap))
              (exit-func (set-transient-map keymap t #'which-key-abort)))
    (define-key keymap [remap keyboard-quit]
      (lambda () (interactive) (funcall exit-func)))
    (which-key--create-buffer-and-show nil keymap)))

(setq prefix-help-command #'repeated-prefix-help-command)
;; Global keybinds:2 ends here

;; [[file:../init.org::*Keys][Keys:1]]
(setq mac-option-modifier 'nil
      mac-command-modifier 'meta)
;;      mac-right-option-modifier 'none))
;; Keys:1 ends here

;; [[file:../init.org::*Path setup][Path setup:1]]
(use-package exec-path-from-shell
  :ensure t
  :if (memq window-system '(mac ns x))
  :config
  (exec-path-from-shell-initialize))
;; Path setup:1 ends here

;; [[file:../init.org::*WSL][WSL:1]]
(when (and (eq system-type 'gnu/linux)
           (string-match-p "microsoft" (shell-command-to-string "uname -a")))
  (setq browse-url-generic-program  "cmd.exe"
	    browse-url-generic-args     '("/c" "start")
	    browse-url-secondary-browser-function #'browse-url-generic
	    browse-url-browser-function #'eww-browse-url)

  (defun wsl-copy (beg end)
    "Copy selected region in WSL"
    (interactive "r")
    (shell-command
     (format "echo %s | clip.exe"
	         (shell-quote-argument (buffer-substring beg end))))))
;; WSL:1 ends here

;; [[file:../init.org::*Odd defaults][Odd defaults:1]]
(setopt confirm-kill-emacs 'y-or-n-p
        use-short-answers t)
;; Odd defaults:1 ends here

;; [[file:../init.org::*Odd defaults][Odd defaults:2]]
(setopt kill-do-not-save-duplicates t
        tab-always-indent 'complete
        tab-bar-tab-hints t
        ring-bell-function 'ignore
        sentence-end-double-space nil
        inhibit-startup-screen t
        set-mark-command-repeat-pop t


        ;; large cursor over tabs
        x-stretch-cursor t

        user-full-name "Janne Holtti"
        user-mail-address "janne@holtiton.fi")
(setopt initial-scratch-message "") ;; Uh, I know what Scratch is for
(setq-default indent-tabs-mode nil
              tab-width 4
              fill-column 79)
;; Odd defaults:2 ends here

;; [[file:../init.org::*Recursive minibuffers][Recursive minibuffers:1]]
(setopt enable-recursive-minibuffers t)
;; Recursive minibuffers:1 ends here

;; [[file:../init.org::*Hide unnecessary command from minibuffer][Hide unnecessary command from minibuffer:1]]
(setopt read-extended-command-predicate #'command-completion-default-include-p)
;; Hide unnecessary command from minibuffer:1 ends here

;; [[file:../init.org::*Scrolling][Scrolling:1]]
(setopt scroll-conservatively 101
        scroll-margin 2
        scroll-preserve-screen-position t)
;; Scrolling:1 ends here

;; [[file:../init.org::*Scrolling][Scrolling:2]]
(setopt scroll-error-top-bottom t)
;; Scrolling:2 ends here

;; [[file:../init.org::*Scrolling][Scrolling:3]]
(setopt next-error-recenter 4)
;; Scrolling:3 ends here

;; [[file:../init.org::*Scrolling][Scrolling:4]]
(setopt fast-but-imprecise-scrolling nil
        jit-lock-defer-time 0)
;; Scrolling:4 ends here

;; [[file:../init.org::*Backups][Backups:1]]
(defvar jh/backup-directory (concat user-emacs-directory "backups"))
(if (not (file-exists-p jh/backup-directory))
        (make-directory jh/backup-directory t))
(setq backup-directory-alist `(("." . ,jh/backup-directory)))
(setq make-backup-files t               ; backup of a file the first time it is saved.
      backup-by-copying t               ; don't clobber symlinks
      version-control t                 ; version numbers for backup files
      delete-old-versions t             ; delete excess backup files silently
      vc-make-backup-files t
      kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
      kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
      auto-save-default t               ; auto-save every buffer that visits a file
      auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
      auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
      )
;; Backups:1 ends here

;; [[file:../init.org::*Trailing whitespace][Trailing whitespace:2]]
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;; Trailing whitespace:2 ends here

;; [[file:../init.org::*Frame resizing][Frame resizing:1]]
(setopt frame-inhibit-implied-resize t)
;; Frame resizing:1 ends here

;; [[file:../init.org::*Multiple windows with ace-window][Multiple windows with ace-window:1]]
(use-package transpose-frame
  :ensure t)

(use-package ace-window
  :ensure t
  :config
  (keymap-global-set "M-o" 'ace-window))
;; Multiple windows with ace-window:1 ends here

;; [[file:../init.org::*Ibuffer projects][Ibuffer projects:1]]
(use-package ibuffer-project
  :ensure t
  :config
  (defun setup-ibuffer-project ()
    "ibuffer-project needs to setup filter groups"
    (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
    (unless (eq ibuffer-sorting-mode 'project-file-relative)
      (ibuffer-do-sort-by-project-file-relative)))
  (add-hook 'ibuffer-hook #'setup-ibuffer-project)

  (setq ibuffer-formats
        '((mark modified read-only locked " "
                (name 30 30 :left :elide) " "
                (size 9 -1 :right) " "
                (mode 16 16 :left :elide) " " project-file-relative)
          (mark " "
                (name 16 -1) " "
                filename))))
;; Ibuffer projects:1 ends here

;; [[file:../init.org::*Editorconfig][Editorconfig:1]]
(use-package editorconfig
  :config
  (editorconfig-mode))
;; Editorconfig:1 ends here

;; [[file:../init.org::*Annotate whatever][Annotate whatever:1]]
(use-package annotate
  :ensure t
  :config
  (annotate-mode))
;; Annotate whatever:1 ends here

;; [[file:../init.org::*More details with marginalia][More details with marginalia:1]]
(use-package marginalia
  :ensure t
  :init
  (marginalia-mode))
;; More details with marginalia:1 ends here

;; [[file:../init.org::*Embark on a journey][Embark on a journey:1]]
(use-package embark
  :ensure t
  :config
  (keymap-global-set "C-." 'embark-act)
  (keymap-global-set "C-;" 'embark-dwim)
  (keymap-global-set "C-h B" 'embark-bindings)

  (keymap-set embark-file-map "t" 'find-file-other-tab)

  (setq prefix-help-command #'embark-prefix-help-command)

  (add-to-list 'display-buffer-alist
	           '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
	             nil
	             (window-parameters (mode-line-format . none)))))
;; Embark on a journey:1 ends here

;; [[file:../init.org::*Artist to unicode][Artist to unicode:1]]
(use-package ascii-art-to-unicode
  :ensure t)
;; Artist to unicode:1 ends here

;; [[file:../init.org::*Automatic environment management with envrc][Automatic environment management with envrc:1]]
(use-package envrc
  :ensure t
  :hook (after-init . envrc-global-mode))
;; Automatic environment management with envrc:1 ends here

;; [[file:../init.org::*Dired][Dired:1]]
(use-package dired
  :config
  ;(setq dired-listing-switches "-AXlh --group-directories-first")
  ;; The below does the same thing as the above (but works on mac)
  (require 'ls-lisp)
  (setq ls-lisp-dirs-first t
        ls-lisp-use-insert-directory-program nil)

  (setq dired-dwim-target t
        dired-hide-details-hide-symlink-targets t)

  (require 'dired-x) ; contains F, which opens marked files

  (add-hook 'dired-mode-hook
      (lambda ()
        (dired-hide-details-mode t))))
;; Dired:1 ends here

;; [[file:../init.org::*Ediff][Ediff:1]]
(use-package ediff-wind
  :config
  (setopt ediff-window-setup-function 'ediff-setup-windows-plain))
;; Ediff:1 ends here

;; [[file:../init.org::*GDB][GDB:1]]
(use-package gdb-mi
  :config
  (setopt gdb-many-windows t))
;; GDB:1 ends here

;; [[file:../init.org::*Savehist][Savehist:1]]
(savehist-mode)
;; Savehist:1 ends here

;; [[file:../init.org::*Winner][Winner:1]]
(winner-mode)
;; Winner:1 ends here

;; [[file:../init.org::*Subwords][Subwords:1]]
(global-subword-mode)
;; Subwords:1 ends here

;; [[file:../init.org::*Delete selection][Delete selection:1]]
(delete-selection-mode)
;; Delete selection:1 ends here

;; [[file:../init.org::*Repeat][Repeat:1]]
(repeat-mode)
;; Repeat:1 ends here

;; [[file:../init.org::*Calendar][Calendar:1]]
(use-package calendar
  :config
  (copy-face font-lock-constant-face 'calendar-iso-week-face)
  (set-face-attribute 'calendar-iso-week-face nil
                      :height 1.0 :foreground "salmon")
  (setq calendar-intermonth-text
        '(propertize
          (format "%2d"
                  (car
                   (calendar-iso-from-absolute
                    (calendar-absolute-from-gregorian (list month day year)))))
          'font-lock-face 'calendar-iso-week-face)
        calendar-mark-diary-entries-flag t
        calendar-date-style 'iso
        calendar-week-start-day 1))
;; Calendar:1 ends here

;; [[file:../init.org::*Spelling with ispell][Spelling with ispell:1]]
(use-package ispell
  :config
  (setq ispell-program-name "hunspell"))
;; Spelling with ispell:1 ends here

;; [[file:../init.org::*Auto revert files][Auto revert files:1]]
(use-package autorevert
  :config
  (global-auto-revert-mode)
  (setq global-auto-revert-non-file-buffers t))
;; Auto revert files:1 ends here

;; [[file:../init.org::*Recentf][Recentf:1]]
(use-package recentf
  :config
  (recentf-mode)
  (setq recentf-max-menu-items 15)
  (setq recentf-max-saved-items 100)
  (add-hook 'buffer-list-update-hook #'recentf-track-opened-file))
;; Recentf:1 ends here

;; [[file:../init.org::*Which key][Which key:1]]
(use-package which-key
  :config
  (dolist (item '(((nil . "calc-") . (nil . "c/"))
                  ((nil . "artist-select-") . (nil . "a/"))
                  ((nil . "org-babel-") . (nil . "ob/"))
                  ))
    (add-to-list 'which-key-replacement-alist item))
  (setopt which-key-sort-order 'which-key-description-order
          which-key-allow-multiple-replacements t)
  (which-key-mode))
;; Which key:1 ends here

;; [[file:../init.org::*Electric pairs][Electric pairs:1]]
(use-package elec-pair
  :config
  (electric-pair-mode))
;; Electric pairs:1 ends here

;; [[file:../init.org::*Menu bar][Menu bar:1]]
(if (and (eq system-type 'darwin)
         (display-graphic-p))
    (menu-bar-mode 1)
  (menu-bar-mode -1))
;; Menu bar:1 ends here

;; [[file:../init.org::*Scroll bar and tool-bar][Scroll bar and tool-bar:1]]
(add-hook 'emacs-startup-hook
          (lambda ()
            (when (display-graphic-p)
              (scroll-bar-mode -1)
              (tool-bar-mode -1))))
;; Scroll bar and tool-bar:1 ends here

;; [[file:../init.org::*Modus themes][Modus themes:1]]
(use-package emacs
  :config
  (require-theme 'modus-themes)
  (setq modus-operandi-palette-overrides
        '((comment red)
          (fnname blue-warmer)
          ;;(keyword fg-main)
          ;;(builtin fg-main)
    	  (string green)))
  (setq modus-vivendi-palette-overrides
        '((bg-main bg-dim)
          (comment red)
    	  (fnname "#71ade7")
          (keyword "#8197bf")
          (builtin "#8fbfdc")
    	  (string green)))

  (setq modus-themes-common-palette-overrides nil)
  (setq modus-themes-italic-constructs t
        modus-themes-mixed-fonts t)

  ;; I don't want the highlighted text to lose the syntax highlighting
  ;; just change the background, use the theme to get a nice value to put there
  (add-hook 'modus-themes-post-load-hook
            (lambda ()
              (set-face-attribute 'region nil
                                  :foreground 'unspecified
                                  :background (cadr (assoc 'bg-active modus-vivendi-palette)))))

  (defun setup-org-src-block-faces ()
    "Need to try to set them like this"
    (setq org-src-block-faces
          '(("emacs-lisp" modus-themes-nuanced-green)
	        ("elisp" modus-themes-nuanced-green)
	        ("scheme" modus-themes-nuanced-green)
	        ("python" modus-themes-nuanced-green)
            ("conf" modus-themes-nuanced-green)
	        ("c" modus-themes-nuanced-blue)
            ("c-ts" modus-themes-nuanced-blue)
	        ("c++" modus-themes-nuanced-blue)
            ("cmake" modus-themes-nuanced-blue)
	        ("js" modus-themes-nuanced-blue)
	        ("sh" modus-themes-nuanced-red)
	        ("shell" modus-themes-nuanced-red)
	        ("restclient" modus-themes-nuanced-red)
	        ("html" modus-themes-nuanced-yellow)
	        ("json" modus-themes-nuanced-yellow)
	        ("xml" modus-themes-nuanced-yellow)
	        ("css" modus-themes-nuanced-yellow))))

  (add-hook 'org-mode-hook #'setup-org-src-block-faces))
;; Modus themes:1 ends here

;; [[file:../init.org::*Zenburn][Zenburn:1]]
(use-package zenburn-theme
  :ensure t
  :config
  ;; use variable-pitch fonts for some headings and titles
  (setq zenburn-use-variable-pitch t)

  ;; scale headings in org-mode
  (setq zenburn-scale-org-headlines t))
;; Zenburn:1 ends here

;; [[file:../init.org::*Gruvbox][Gruvbox:1]]
(use-package gruvbox-theme
  :ensure t)
;; Gruvbox:1 ends here

;; [[file:../init.org::*Stimmung][Stimmung:1]]
(use-package stimmung-themes
  :ensure t
  :config
  (setq stimmung-themes-dark-highlight-color-foreground "#71ade7"
        stimmung-themes-function-name 'foreground))
;; Stimmung:1 ends here

;; [[file:../init.org::*Everforest][Everforest:1]]
(use-package everforest-theme
  :vc (:url "https://github.com/Theory-of-Everything/everforest-emacs"
            :rev newest))
;; Everforest:1 ends here

;; [[file:../init.org::*Jellybeans][Jellybeans:1]]
(use-package jbeans-theme
  :ensure t)
;; Jellybeans:1 ends here

;; [[file:../init.org::*EF themes][EF themes:1]]
(use-package ef-themes
  :ensure t)
;; EF themes:1 ends here

;; [[file:../init.org::*Standard themes][Standard themes:1]]
(use-package standard-themes
  :ensure t
  :config
  (setq standard-themes-italic-constructs t
        standard-themes-mixed-fonts t)

  (setq standard-dark-palette-overrides
        '((keyword blue-cooler)
          (bg-main bg-dim))))
;; Standard themes:1 ends here

;; [[file:../init.org::*Standard themes][Standard themes:2]]
(defun set-org-keyword-faces ()
  (setq org-todo-keyword-faces
        `(("TODO" . (:foreground ,(standard-themes-get-color-value 'blue-warmer) :weight bold))
          ("NEXT" . (:foreground ,(standard-themes-get-color-value 'magenta-warmer) :weight bold))
          ("IN-PROGRESS" . (:foreground ,(standard-themes-get-color-value 'yellow-warmer) :weight bold))
          ("BLOCKED" . (:foreground ,(standard-themes-get-color-value 'red-warmer) :weight bold))
          ("DONE" . (:foreground ,(standard-themes-get-color-value 'green) :weight bold))

          ("PLANNING" . (:foreground ,(standard-themes-get-color-value 'blue-cooler) :weight bold))
          ("ACTIVE" . (:foreground ,(standard-themes-get-color-value 'yellow-cooler) :weight bold))
          ("ON-HOLD" . (:foreground ,(standard-themes-get-color-value 'red-faint) :weight bold))
          ("FINISHED" . (:foreground ,(standard-themes-get-color-value 'green-cooler) :weight bold))

          ("CANCELLED" . (:foreground ,(standard-themes-get-color-value 'fg-dim) :weight bold)))))

(add-hook 'standard-themes-after-load-theme-hook #'set-org-keyword-faces)
;; Standard themes:2 ends here

;; [[file:../init.org::*Blue stuff][Blue stuff:1]]
(use-package tomorrow-night-deepblue-theme
  :ensure t)
;; Blue stuff:1 ends here

;; [[file:../init.org::*Solarized][Solarized:1]]
(use-package solarized-theme
  :ensure t)
;; Solarized:1 ends here

;; [[file:../init.org::*Current theme][Current theme:1]]
(load-theme 'standard-dark t)
;; Current theme:1 ends here

;; [[file:../init.org::*Mac][Mac:1]]
;; (setq default-font "SF Mono")
;; (setq variable-font "ETBembo")
;; (setq mono-font "Iosevka")
(when (eq system-type 'darwin)
  (setq mono-font "RobotoMono Nerd Font")
  (setq default-font "RobotoMono Nerd Font")
  (setq variable-font "Roboto Slab")

  (set-face-attribute 'default nil :font default-font :weight 'light :height 160)
  (set-face-attribute 'bold nil :weight 'regular)
  (set-face-attribute 'bold-italic nil :weight 'regular)
  (set-face-attribute 'fixed-pitch nil :font mono-font :weight 'light :height 160)
  (set-face-attribute 'variable-pitch nil :family variable-font :weight 'light :height 190))
;; Mac:1 ends here

;; [[file:../init.org::*Linux][Linux:1]]
;; (setq default-font "FiraCode Nerd Font Mono")
(when (and (eq system-type 'gnu/linux)
           (not (string-match-p "microsoft" (shell-command-to-string "uname -a"))))
  (setq mono-font "Iosevka")
  (setq variable-font "Alegreya")
  (setq default-font "Source Code Pro")

  (set-face-attribute 'default nil :font default-font :weight 'regular :slant 'normal :height 120)
  (set-face-attribute 'fixed-pitch nil :font mono-font :weight 'regular :slant 'normal :height 120)
  (set-face-attribute 'variable-pitch nil :font variable-font :weight 'regular :height 140))
;; Linux:1 ends here

;; [[file:../init.org::*Fixed pitch][Fixed pitch:1]]
(use-package fixed-pitch
  :vc (:url "https://github.com/cstby/fixed-pitch-mode"
            :rev :newest)
  :diminish
  :config
  (fixed-pitch-mode))
;; Fixed pitch:1 ends here

;; [[file:../init.org::*Support mouse on CLI][Support mouse on CLI:1]]
(add-hook 'emacs-startup-hook
          (lambda ()
            (unless (display-graphic-p)
              (xterm-mouse-mode 1))))
;; Support mouse on CLI:1 ends here

;; [[file:../init.org::*Diminish and Minions][Diminish and Minions:1]]
(use-package diminish
  :ensure t
  :config
  (diminish 'subword-mode)
  (diminish 'visual-line-mode)
  (diminish 'fixed-pitch-mode)
  (add-hook 'buffer-face-mode-hook (lambda () (diminish 'buffer-face-mode))))
;; Diminish and Minions:1 ends here

;; [[file:../init.org::*Diminish and Minions][Diminish and Minions:2]]
(use-package minions
  :ensure t
  :config
  (minions-mode))
;; Diminish and Minions:2 ends here

;; [[file:../init.org::*Doom modeline][Doom modeline:1]]
(use-package doom-modeline
  :ensure t
  :config
  (setq doom-modeline-minor-modes t)
  (setq doom-modeline-buffer-file-name-style 'file-name-with-project)
  (doom-modeline-mode))
;; Doom modeline:1 ends here

;; [[file:../init.org::*Nerd icons][Nerd icons:1]]
(use-package nerd-icons
  :ensure t)
;; Nerd icons:1 ends here

;; [[file:../init.org::*Dired support with nerd icons][Dired support with nerd icons:1]]
(use-package nerd-icons-dired
  :ensure t
  :after nerd-icons
  :hook (dired-mode . nerd-icons-dired-mode))
;; Dired support with nerd icons:1 ends here

;; [[file:../init.org::*Popper][Popper:1]]
(use-package popper
  :ensure t
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          help-mode
          compilation-mode
          "^\\*eldoc*\\*$"
          "^\\*eshell.*\\*$" eshell-mode ;eshell as a popup
          "^\\*shell.*\\*$"  shell-mode  ;shell as a popup
          "^\\*term.*\\*$"   term-mode   ;term as a popup
          "^\\*vterm.*\\*$"   vterm-mode   ;vterm as a popup
          ))
  ;; (setq popper-group-function #'popper-group-by-project)
  (popper-mode +1)
  (popper-echo-mode +1)
  (keymap-global-set "C-`" 'popper-toggle)
  (keymap-global-set "M-`" 'popper-cycle)
  (keymap-global-set "C-M-`" 'popper-toggle-type))
;; Popper:1 ends here

;; [[file:../init.org::*Breadcrumbs][Breadcrumbs:1]]
(use-package breadcrumb
  :ensure t
  :config
  (breadcrumb-mode 1)
  (setq breadcrumb-imenu-crumbs 0.5))
;; Breadcrumbs:1 ends here

;; [[file:../init.org::*Auto-dim other buffers][Auto-dim other buffers:1]]
(use-package auto-dim-other-buffers
  :ensure t
  :config
  (auto-dim-other-buffers-mode))
;; Auto-dim other buffers:1 ends here

;; [[file:../init.org::*Highlight todos][Highlight todos:1]]
(use-package hl-todo
  :ensure t
  :config
  (add-hook 'prog-mode-hook (lambda () (hl-todo-mode))))
;; Highlight todos:1 ends here

;; [[file:../init.org::*Rainbow delimiters][Rainbow delimiters:1]]
(use-package rainbow-delimiters
  :ensure t)
;; Rainbow delimiters:1 ends here

;; [[file:../init.org::*Highlight changes in gutter][Highlight changes in gutter:1]]
(use-package diff-hl
  :ensure t
  :config
  (setopt diff-hl-draw-borders nil)
  (global-diff-hl-mode))
;; Highlight changes in gutter:1 ends here

;; [[file:../init.org::*Make the current window 70 percent][Make the current window 70 percent:1]]
(defun window-to-70-percent ()
  (interactive)
  (if (eql (prefix-numeric-value current-prefix-arg) 4)
      (window-resize nil (- (truncate (* 0.7 (frame-width))) (window-width)) t)
    (window-resize nil (- (truncate (* 0.7 (frame-height))) (window-height)))))

(keymap-global-set "C-x w 7" 'window-to-70-percent)
;; Make the current window 70 percent:1 ends here

;; [[file:../init.org::*Vertico][Vertico:1]]
(use-package vertico
  :ensure t
  :init
  (vertico-mode))
;; Vertico:1 ends here

;; [[file:../init.org::*Vertico-reverse][Vertico-reverse:1]]
(use-package vertico-reverse
  :after vertico
  :ensure nil ;; built-in
  :init (vertico-reverse-mode))
;; Vertico-reverse:1 ends here

;; [[file:../init.org::*Vertico-multiform][Vertico-multiform:1]]
(use-package vertico-multiform
  :after vertico
  :ensure nil ;; built-in
  :init (vertico-multiform-mode)
  :config
  (setopt vertico-multiform-categories
          '((embark-keybinding reverse))))
;; Vertico-multiform:1 ends here

;; [[file:../init.org::*Orderless][Orderless:1]]
(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless basic)))
;; Orderless:1 ends here

;; [[file:../init.org::*Consult][Consult:1]]
(use-package consult
  :ensure t
  :bind (;; C-c bindings in `mode-specific-map'
         ("C-c M-x" . consult-mode-command)
         ("C-c h" . consult-history)
         ("C-c k" . consult-kmacro)
         ("C-c m" . consult-man)
         ("C-c i" . consult-info)
         ([remap Info-search] . consult-info)
         ;; C-x bindings in `ctl-x-map'
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x t b" . consult-buffer-other-tab)    ;; orig. switch-to-buffer-other-tab
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ;; M-g bindings in `goto-map'
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings in `search-map'
         ("M-s d" . consult-fd)                  ;; Alternative: consult-fd
         ("M-s c" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history)                ;; orig. previous-matching-history-element
       )
  :config
  ;; Narrowing lets you restrict results to certain groups of candidates
  (setq consult-narrow-key "<")
  ;; better register integration
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)
  (advice-add #'register-preview :override #'consult-register-window)
  ;; use consult for xref
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref))
;; Consult:1 ends here

;; [[file:../init.org::*Consult][Consult:2]]
(use-package consult-todo
  :ensure t
  :after (consult)
  :config
  (keymap-set search-map "t" 'consult-todo)
  (keymap-set search-map "T" 'consult-todo-project))
;; Consult:2 ends here

;; [[file:../init.org::*Consult][Consult:3]]
(use-package embark-consult
  :ensure t
  :after (consult embark)
  :config
  (add-hook 'embark-collect-mode-hook 'consult-preview-at-point-mode))
;; Consult:3 ends here

;; [[file:../init.org::*Jumping within a frame][Jumping within a frame:1]]
(use-package avy
  :ensure t
  :init (setq avy-background t)
  :config
  (keymap-global-set "C-c j" 'avy-goto-char-timer))
;; Jumping within a frame:1 ends here

;; [[file:../init.org::*Jumping around without LSP][Jumping around without LSP:1]]
(use-package dumb-jump
  :ensure t
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate)
  (setopt dumb-jump-force-searcher 'rg))
;; Jumping around without LSP:1 ends here

;; [[file:../init.org::*Ripgrep][Ripgrep:1]]
(use-package rg
  :ensure t
  :config
  (rg-enable-default-bindings))
;; Ripgrep:1 ends here

;; [[file:../init.org::*Ripgrep][Ripgrep:2]]
(setopt grep-command "rg -nS --no-heading "
        grep-use-null-device nil
        xref-search-program 'ripgrep)
;; Ripgrep:2 ends here

;; [[file:../init.org::*Editable grep buffers][Editable grep buffers:1]]
(use-package wgrep
  :ensure t)
;; Editable grep buffers:1 ends here

;; [[file:../init.org::*Isearch][Isearch:1]]
(use-package isearch
  :config
  (setq isearch-lazy-count t
        search-whitespace-regexp ".*?"))
;; Isearch:1 ends here

;; [[file:../init.org::*Completion][Completion:1]]
(setopt completions-max-height 20
        completion-category-defaults nil
        completions-format 'one-column
        completion-auto-select 'second-tab
        completion-auto-help 'visible
        ;;completions-sort 'historical
        ;;completions-detailed t
        completion-category-overrides '((file (styles basic partial-completion))))
;; Completion:1 ends here

;; [[file:../init.org::*Corfu][Corfu:1]]
(use-package corfu
  :ensure t
  :init
  (global-corfu-mode)
  :bind
  (:map corfu-map
        ("SPC" . corfu-insert-separator)
        ("C-n" . corfu-next)
        ("C-p" . corfu-previous)))

;; Part of corfu
(use-package corfu-popupinfo
  :after (corfu)
  :ensure nil
  :hook (corfu-mode . corfu-popupinfo-mode)
  :custom
  (corfu-popupinfo-delay '(0.25 . 0.1))
  (corfu-popupinfo-hide nil)
  :config
  (corfu-popupinfo-mode))
;; Corfu:1 ends here

;; [[file:../init.org::*Corfu][Corfu:2]]
(use-package corfu-terminal
  :if (not (display-graphic-p))
  :after (corfu)
  :ensure t
  :config
  (corfu-terminal-mode))
;; Corfu:2 ends here

;; [[file:../init.org::*Cape][Cape:1]]
(use-package cape
  :ensure t
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file))
;; Cape:1 ends here

;; [[file:../init.org::*Cape][Cape:2]]
(use-package kind-icon
  :if (display-graphic-p)
  :ensure t
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))
;; Cape:2 ends here

;; [[file:../init.org::*Yasnippet][Yasnippet:1]]
(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :ensure t)
;; Yasnippet:1 ends here

;; [[file:../init.org::*Yasnippet][Yasnippet:2]]
(use-package yasnippet-capf
  :vc (:url "https://github.com/elken/yasnippet-capf"
            :rev :newest)
  :config
  (keymap-set mode-specific-map "p y" 'yasnippet-capf)
  (add-to-list 'completion-at-point-functions #'yasnippet-capf))
;; Yasnippet:2 ends here

;; [[file:../init.org::*Expand region][Expand region:1]]
(use-package expand-region
  :ensure t
  :config
  (keymap-global-set "C-=" 'er/expand-region))
;; Expand region:1 ends here

;; [[file:../init.org::*Multiple cursors][Multiple cursors:1]]
(use-package multiple-cursors
  :ensure t
  :config
  (keymap-global-set "C-S-c C-S-c" 'mc/edit-lines))
;; Multiple cursors:1 ends here

;; [[file:../init.org::*CSV][CSV:1]]
(use-package csv-mode
  :ensure t)
;; CSV:1 ends here

;; [[file:../init.org::*Flymake][Flymake:1]]
(use-package flymake
  :config
  (eval-after-load "flymake"
    '(progn
       (keymap-set flymake-mode-map "M-n" 'flymake-goto-next-error)
       (keymap-set flymake-mode-map "M-p" 'flymake-goto-prev-error))))
;; Flymake:1 ends here

;; [[file:../init.org::*Eglot][Eglot:1]]
(use-package eglot
  :disabled t
  :config
  ;; this just removes the remap that looks horrible with the annotations in fido/icomplete
  (define-key eglot-mode-map [remap display-local-help] nil)
  (define-key prog-mode-map (kbd "C-c e") eglot-mode-map)
  (keymap-set eglot-mode-map "r" 'eglot-rename)
  (keymap-set eglot-mode-map "f" 'eglot-format)
  (keymap-set eglot-mode-map "e" 'eglot-code-actions)
  (keymap-set eglot-mode-map "h" 'eldoc)
  (keymap-set eglot-mode-map "a" 'eglot-find-implementation)

  (fset #'jsonrpc--log-event #'ignore))
;; Eglot:1 ends here

;; [[file:../init.org::*Line numbers in prog-mode buffers][Line numbers in prog-mode buffers:1]]
(use-package prog-mode
  :hook (prog-mode . (lambda ()
	                   (setq display-line-numbers t))))
;; Line numbers in prog-mode buffers:1 ends here

;; [[file:../init.org::*Compilation buffer colors][Compilation buffer colors:1]]
(require 'ansi-color)
(add-hook 'compilation-filter-hook #'ansi-color-compilation-filter)
;; Compilation buffer colors:1 ends here

;; [[file:../init.org::*Treesitter][Treesitter:1]]
(require 'treesit) ; needed for `treesit-language-source-alist'
;; Treesitter:1 ends here

;; [[file:../init.org::*All][All:1]]
(use-package c-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(c "https://github.com/tree-sitter/tree-sitter-c"))
  (add-to-list 'treesit-language-source-alist '(doxygen "https://github.com/tree-sitter-grammars/tree-sitter-doxygen"))

  (dolist (lang '(c doxygen))
    (unless (treesit-language-available-p lang)
      (treesit-install-language-grammar lang)))

  (defun my-c-indent-style()
    "Override the built-in indentation style with some additional rules"
    `(;; overrides
      ((node-is "case") parent-bol c-ts-mode-indent-offset)
      ;; base
      ,@(alist-get 'bsd (c-ts-mode--indent-styles 'cpp))))

  :config
  (setq c-ts-mode-indent-offset 4
        c-ts-mode-indent-style #'my-c-indent-style
        c-ts-mode-enable-doxygen t))

(use-package c++-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(cpp "https://github.com/tree-sitter/tree-sitter-cpp"))

  (unless (treesit-language-available-p 'cpp)
    (treesit-install-language-grammar 'cpp)))

(use-package asm-mode
  :init
  (defun fix-asm-mode ()
    ;; you can use `comment-dwim' (M-;) for this kind of behaviour anyway
    (local-unset-key (vector asm-comment-char))
    (electric-indent-mode nil)
    ;; asm-mode sets it locally to nil, to "stay closer to the old TAB behaviour".
    (setq tab-always-indent (default-value 'tab-always-indent)))
  :hook (asm-mode . #'fix-asm-mode))

(use-package rust-ts-mode
  :disabled
  :init
  (add-to-list 'treesit-language-source-alist '(rust "https://github.com/tree-sitter/tree-sitter-rust"))

  (unless (treesit-language-available-p 'rust)
    (treesit-install-language-grammar 'rust)))

(use-package rust-mode
  :ensure t)

(use-package csharp-mode
  :init
  (add-to-list 'treesit-language-source-alist '(c-sharp "https://github.com/tree-sitter/tree-sitter-c-sharp"))

  (unless (treesit-language-available-p 'c-sharp)
    (treesit-install-language-grammar 'c-sharp))

  (when (treesit-ready-p 'c-sharp)
    (add-to-list 'major-mode-remap-defaults '(csharp-mode . csharp-ts-mode)))

  (with-eval-after-load 'eglot
    (dolist (mode-server-pair '((csharp-ts-mode . ("OmniSharp" "-lsp"))))
      (add-to-list 'eglot-server-programs mode-server-pair))))

(use-package mhtml-mode
  :mode ("\\.cshtml?\\'"))

(use-package html-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(html "https://github.com/tree-sitter/tree-sitter-html"))

  (unless (treesit-language-available-p 'html)
    (treesit-install-language-grammar 'html)))

(use-package css-mode
  :init
  (add-to-list 'treesit-language-source-alist '(css "https://github.com/tree-sitter/tree-sitter-css"))

  (unless (treesit-language-available-p 'css)
    (treesit-install-language-grammar 'css)))

(use-package elixir-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(elixir "https://github.com/elixir-lang/tree-sitter-elixir"))
  (add-to-list 'treesit-language-source-alist '(heex "https://github.com/phoenixframework/tree-sitter-heex"))

  (dolist (lang '(elixir heex))
    (unless (treesit-language-available-p lang)
      (treesit-install-language-grammar lang)))

  (with-eval-after-load 'eglot
    (add-to-list 'eglot-server-programs '(elixir-mode "~/bin/elixir-ls/language_server.sh"))))

(use-package typescript-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src"))
  (add-to-list 'treesit-language-source-alist '(tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src"))

  (dolist (lang '(typescript tsx))
    (unless (treesit-language-available-p lang)
      (treesit-install-language-grammar lang)))

  :config
  (setq typescript-ts-mode-indent-offset 2))

(use-package js
  :init
  ;; includes both js and jsx grammars
  (add-to-list 'treesit-language-source-alist '(javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src"))
  (add-to-list 'treesit-language-source-alist '(jsdoc "https://github.com/tree-sitter/tree-sitter-jsdoc" "master" "src"))

  (dolist (lang '(javascript jsdoc))
    (unless (treesit-language-available-p lang)
      (treesit-install-language-grammar lang))))

(use-package json-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(json "https://github.com/tree-sitter/tree-sitter-json"))

  (unless (treesit-language-available-p 'json)
    (treesit-install-language-grammar 'json)))

(use-package dockerfile-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(dockerfile "https://github.com/camdencheek/tree-sitter-dockerfile"))

  (unless (treesit-language-available-p 'dockerfile)
    (treesit-install-language-grammar 'dockerfile)))

(use-package yaml-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(yaml "https://github.com/ikatyang/tree-sitter-yaml"))

  (unless (treesit-language-available-p 'yaml)
    (treesit-install-language-grammar 'yaml)))

(use-package toml-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(toml "https://github.com/ikatyang/tree-sitter-toml"))

  (unless (treesit-language-available-p 'toml)
    (treesit-install-language-grammar 'toml)))

(use-package cmake-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(cmake "https://github.com/uyha/tree-sitter-cmake"))

  (unless (treesit-language-available-p 'cmake)
    (treesit-install-language-grammar 'cmake))

  :config
  (setq cmake-ts-mode-indent-offset 2))

(use-package bash-ts-mode
  :mode "\\sh.\\'"
  :init
  (add-to-list 'treesit-language-source-alist '(bash "https://github.com/tree-sitter/tree-sitter-bash"))

  (unless (treesit-language-available-p 'bash)
    (treesit-install-language-grammar 'bash)))

(use-package python
  :init
  (add-to-list 'treesit-language-source-alist '(python "https://github.com/tree-sitter/tree-sitter-python"))
  (unless (treesit-language-available-p 'python)
    (treesit-install-language-grammar 'python))

  (when (treesit-ready-p 'python)
    (add-to-list 'major-mode-remap-defaults '(python-mode . python-ts-mode)))

  (with-eval-after-load 'eglot
    (add-to-list 'eglot-server-programs
                 `((python-mode python-ts-mode) . ,(eglot-alternatives '(("poetry" "run" "pylsp")
                                                                         "pylsp"
                                                                         "pyls"
                                                                         ("poetry" "run" "pyright-langserver" "--stdio")
                                                                         ("pyright-langserver" "--stdio")
                                                                         "jedi-language-server"))))))

(use-package go-ts-mode
  :init
  (add-to-list 'treesit-language-source-alist '(go "https://github.com/tree-sitter/tree-sitter-go"))
  (add-to-list 'treesit-language-source-alist '(gomod "https://github.com/camdencheek/tree-sitter-go-mod"))

  (dolist (lang '(go gomod))
    (unless (treesit-language-available-p lang)
      (treesit-install-language-grammar lang)))

  :config
  (setq go-ts-mode-indent-offset 4))

;;; external packages
(use-package zig-mode
  :ensure t)

(use-package robot-mode
  :ensure t)

(use-package paredit
  :ensure t)

(use-package geiser-chez
  :ensure t)

(use-package sly
  :ensure t
  :config
  (setq inferior-lisp-program "sbcl"))

(use-package plantuml-mode
  :ensure t
  :config
  (setq plantuml-default-exec-mode 'executable))

(use-package gleam-ts-mode
  :ensure t
  :mode (rx ".gleam" eos)
  :init
  (add-to-list 'treesit-language-source-alist '(gleam "https://github.com/gleam-lang/tree-sitter-gleam"))

  (unless (treesit-language-available-p 'gleam)
    (treesit-install-language-grammar 'gleam))

  :config
  (require 'eglot)
  (add-to-list 'eglot-server-programs
               '(gleam-ts-mode . ("gleam" "lsp"))))
;; All:1 ends here

;; [[file:../init.org::*Org babel - Elisp][Org babel - Elisp:1]]
(use-package org
  :config
  (add-to-list 'org-babel-default-header-args:elisp
               '(:comments . "link")))
;; Org babel - Elisp:1 ends here

;; [[file:../init.org::*Magit][Magit:1]]
(use-package magit
  :ensure t
  :config
  (require 'bookmark)
  (setopt magit-repository-directories
        `((,(bookmark-get-filename (car (member "Projects" (bookmark-all-names)))) . 1)))
  (setopt magit-format-file-function 'magit-format-file-nerd-icons))
;; Magit:1 ends here

;; [[file:../init.org::*Vterm][Vterm:1]]
(use-package vterm
  :ensure t)
;; Vterm:1 ends here

;; [[file:../init.org::*Visual undo][Visual undo:1]]
(use-package vundo
  :ensure t)
;; Visual undo:1 ends here

;; [[file:../init.org::*Ollama with gptel][Ollama with gptel:1]]
(use-package gptel
  :ensure t
  :config
  (setq gptel-default-mode 'org-mode
        gptel-model 'llama3.1:latest
        gptel-backend (gptel-make-ollama "Ollama"
                        :host "localhost:11434"
                        :stream t
                        :models '(llama3.1:latest))))
;; Ollama with gptel:1 ends here

;; [[file:../init.org::*Epilogue][Epilogue:1]]
(provide 'init-main)
;; Epilogue:1 ends here
