;;; gleam-ts-mode.el --- tree-sitter based major mode for Gleam -*- lexical-binding: t -*-

;;; Commentary:

;; 

;;; Code:

(require 'treesit)

(declare-function treesit-parser-create "treesit.c")
(declare-function treesit-node-child-by-field-name "treesit.c")
(declare-function treesit-node-type "treesit.c")

;;; Customization
(defgroup gleam-ts nil
  "Major mode for Gleam"
  :prefix "gleam-ts-"
  :group 'languages)

(defcustom gleam-ts-mode-hook nil
  "Hook that runs when gleam-ts-mode starts."
  :type 'hook
  :group 'gleam-ts)

(defcustom gleam-ts-mode-indent-offset 2
  "Number of space for each indentation step in `gleam-ts-mode'."
  :type 'integer
  :group 'gleam-ts)

(defvar gleam-ts-mode-map
  (let ((map (make-sparse-keymap)))
    map)
  "Keymap used in gleam-mode buffers.")

(defvar gleam-ts-mode--font-lock-settings
  (treesit-font-lock-rules
   :default-language 'gleam

   :feature 'delimiter
   '(["." "," ":" "#" "=" "->" ".." "-" "<-"] @font-lock-delimiter-face)

   :feature 'bracket
   '(["(" ")" "[" "]" "{" "}" "<<" ">>"] @font-lock-bracket-face)

   :feature 'keyword
   '(["as" "assert" "case" "const" "fn" "if" "import" "let" "panic" "todo" "type" "use"] @font-lock-keyword-face)

   :feature 'comment
   '((comment) @font-lock-comment-face
     (module_comment) @font-lock-comment-face
     (statement_comment) @font-lock-comment-face)

   :feature 'string
   '((string (quoted_content)) @font-lock-string-face)

   :feature 'constant
   '((constant name: (identifier) @font-lock-constant-face))

   :feature 'definition
   '((function name: (identifier) @font-lock-function-name-face)
     (function (visibility_modifier) name: (identifier) @font-lock-function-name-face))

   :feature 'number
   '([(integer) (float)] @font-lock-number-face)

   :feature 'function-call
   '(
     ((function_call function: (identifier) @font-lock-function-call-face))
     )

   )
  "Tree-sitter font-lock settings for `gleam-ts-mode'.")

(defvar gleam-ts-mode--indent-rules
  `((gleam
     ((parent-is "source_code") column-0 0)
     ((node-is "]") parent-bol 0)
     ((node-is ")") parent-bol 0)
     ((node-is "}") parent-bol 0)))
  "Tree-sitter indent rules for `gleam-ts-mode'.")

;;;###autoload
(define-derived-mode gleam-ts-mode prog-mode "Gleam"
  "Major mode for Gleam.

Key bindings:
\\{gleam-mode-map}"
  :group 'gleam-ts
 
  (when (treesit-ready-p 'gleam)
    (treesit-parser-create 'gleam)

    ;; Font lock
    (setq-local treesit-font-lock-settings
                gleam-ts-mode--font-lock-settings)

    (setq-local treesit-font-lock-feature-list
                '((comment definition)
                  (keyword string)
                  (number function-call)
                  (bracket delimiter)))

    ;; Indent
    (setq-local indent-tabs-mode nil)
    (setq-local treesit-simple-indent-rules gleam-ts-mode--indent-rules)

    ;; Register compilation error format
    (add-to-list 'compilation-error-regexp-alist-alist '(gleam "┌─ \\([^ ]+\\.gleam\\):\\([0-9]+\\):\\([0-9]+\\)" 1 2 3))
    (add-to-list 'compilation-error-regexp-alist 'gleam)

    ;; Imenu settings
    (setq-local imenu-generic-expression
                '(("Private Constants"          "^\\s-*const\\s-+\\([_a-z][_0-9a-z]*\\)\\s-+.*$" 1)
                  ("Public Constants"           "^\\s-*pub\\s-+const\\s-+\\([_a-z][_0-9a-z]*\\)\\s-+.*$" 1)
                  ("Private External Types"     "^\\s-*external\\s-+type\\s-+\\([A-Z][_0-9a-zA-Z]*\\).*$" 1)
                  ("Public External Types"      "^\\s-*pub\\s-+external\\s-+type\\s-+\\([A-Z][_0-9a-zA-Z]*\\).*$" 1)
                  ("Private External Functions" "^\\s-*external\\s-+fn\\s-+\\([_a-z][_0-9a-z]*\\).*$" 1)
                  ("Public External Functions"  "^\\s-*pub\\s-+external\\s-+fn\\s-+\\([_a-z][_0-9a-z]*\\).*$" 1)
                  ("Private Functions"          "^\\s-*fn\\s-+\\([_a-z][_0-9a-z]*\\).*$" 1)
                  ("Public Functions"           "^\\s-*pub\\s-+fn\\s-+\\([_a-z][_0-9a-z]*\\).*$" 1)
                  ("Private Types"              "^\\s-*type\\s-+\\([A-Z][_0-9a-zA-Z]*\\).*$" 1)
                  ("Public Types"               "^\\s-*pub\\s-+type\\s-+\\([A-Z][_0-9a-zA-Z]*\\).*$" 1)
                  ("Public Opaque Types"        "^\\s-*pub\\s-+opaque\\s-+type\\s-+\\([A-Z][_0-9a-zA-Z]*\\).*$" 1)))
    (setq-local imenu-case-fold-search nil)
  
    (treesit-major-mode-setup)))


;;; Public functions
(defun gleam-format ()
  "Format the current buffer using the `gleam format' command."
  (interactive)
  (if (executable-find "gleam")
    (save-restriction ; Save the user's narrowing, if any
      (widen)         ; Expand scope to the whole, unnarrowed buffer
      (let* ((buf (current-buffer))
             (min (point-min))
             (max (point-max))
             (tmpfile (make-nearby-temp-file "gleam-format")))
        (unwind-protect
            (with-temp-buffer
              (insert-buffer-substring-no-properties buf min max)
              (write-file tmpfile)
              (call-process "gleam" nil nil nil "format" (buffer-file-name))
              (revert-buffer :ignore-autosave :noconfirm)
              (let ((tmpbuf (current-buffer)))
                (with-current-buffer buf
                  (replace-buffer-contents tmpbuf))))
          (if (file-exists-p tmpfile) (delete-file tmpfile)))
        (message "Formatted!")))
    (message "`gleam' executable not found!")))

(if (treesit-ready-p 'gleam)
    (add-to-list 'auto-mode-alist '("\\.gleam\\'" . gleam-ts-mode)))

(provide 'gleam-ts-mode)
;;; gleam-ts-mode.el ends here
