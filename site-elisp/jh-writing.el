;;; -*- lexical-binding: t; -*-

(use-package markdown-mode
  :ensure t
  :config
  (setq markdown-fontify-code-blocks-natively t))

(dolist (mode '(flyspell-mode visual-line-mode))
  (add-hook 'org-mode-hook mode)
  (add-hook 'markdown-mode-hook mode))

(use-package org-ql
  :ensure t
  :bind ("C-c q" . org-ql-view)
  :config
  (add-to-list 'org-ql-views
               '("Overview: Tasks per project"
                 :buffers-files org-agenda-files
                 :query (and (todo) (ancestors (todo "PLANNING" "ACTIVE")))
                 :title "Overview: Project tasks"
                 :sort (todo)
                 :super-groups ((:auto-parent t))))
  (add-to-list 'org-ql-views
               '("Overview: Projects"
                 :buffers-files org-agenda-files
                 :query (todo "PLANNING" "ACTIVE" "ON-HOLD")
                 :title "Overview: Projects"
                 :sort (todo priority)
                 :super-groups ((:auto-parent t))))
  (add-to-list 'org-ql-views
               '("Review: Stuck projects"
                 :buffers-files org-agenda-files
                 :query (and (todo "PLANNING" "ACTIVE") (not (done))
                             (not (descendants (todo "NEXT" "IN-PROGRESS")))
                             (not (descendants (scheduled))))
                 :title "Review: Stuck projects"
                 :sort (date priority)
                 :super-groups nil
                 :narrow nil))
  (add-to-list 'org-ql-views
               '("Overview: Stand-alone tasks"
                 :buffers-files org-agenda-files
                 :query (and (todo "TODO" "IN-PROGRESS" "BLOCKED")
                             (not (ancestors (todo "PLANNING" "ACTIVE"))))
                 :title "Overview: Stand-alone tasks"
                 :sort (todo priority)
                 :super-groups ((:auto-parent t)))))

(use-package org-super-agenda
  :ensure t
  :config
  (org-super-agenda-mode)
  (setq org-super-agenda-groups
        '((:log t)  ; Automatically named "Log"
          (:name "Schedule"
                 :time-grid t)
          (:name "Today"
                 :scheduled today)
          (:habit t)
          (:name "Due today"
                 :deadline today)
          (:name "Overdue"
                 :deadline past)
          (:name "Due soon"
                 :deadline future)
          (:name "Unimportant"
                 :todo ("SOMEDAY" "MAYBE" "CHECK" "TO-READ" "TO-WATCH")
                 :order 100)
          (:name "Waiting..."
                 :todo "WAITING"
                 :order 98)
          (:name "Scheduled earlier"
                 :scheduled past))))

(setq org-agenda-custom-commands
      '(("p" . "Projects")
        ("pp" "List projects"
         ((org-ql-block '(todo "PLANNING" "ACTIVE" "ON-HOLD")
                        ((org-ql-block-header "Projects")))))
        ("pt" "Task per project"
         ((org-ql-block  '(and (todo)
                               (ancestors (todo "PLANNING" "ACTIVE" "ON-HOLD")))
                         ((org-ql-block-header "Project tasks")))))
        ("ps" "Stuck Projects"
         ((org-ql-block '(and (todo "PLANNING" "ACTIVE")
                              (not (done))
                              (not (descendants (todo "NEXT" "IN-PROGRESS")))
                              (not (descendants (scheduled))))
                        ((org-ql-block-header "Stuck Projects")))))))

(use-package bookmark)
(setq org-directory (bookmark-get-filename (car (member "notes" (bookmark-all-names)))))
(setq org-default-notes-file (concat org-directory "notes.org"))

(setq org-enforce-todo-dependencies t)
(setq org-agenda-dim-blocked-tasks nil)
(setq org-imenu-depth 3)
(setq org-startup-indented t)
(setq org-log-into-drawer t)
(setq org-log-done 'note)
(setq org-stuck-projects '("TODO=\"ACTIVE\"" ("NEXT" "IN-PROGRESS") nil ""))
(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT" "IN-PROGRESS(i@/!)" "BLOCKED(b@/!)" "|" "DONE(d@/!)" "CANCELLED(c@/!)")
        (sequence "PLANNING(p)" "ACTIVE(a@/!)" "ON-HOLD(o@/!)" "|" "FINISHED(f@/!)" "CANCELLED(c@/!)")
        ))

(setq org-catch-invisible-edits 'show-and-error)

(use-package org
  :bind ("C-c l" . org-store-link))

(setq org-special-ctrl-a/e t)
(setq org-use-speed-commands t)
(setq org-return-follows-link t)

(setq org-goto-interface 'outline-path-completion)
(setq org-goto-max-level 5)
(setq org-outline-path-complete-in-steps nil)
(setq org-refile-use-outline-path 'file)
(setq org-refile-targets '((org-agenda-files . (:maxlevel . 5))))

;;; work-around  for org-ctags obnoxious behavior
(with-eval-after-load 'org-ctags (setq org-open-link-functions nil))

(setq org-export-backends '(html latex md odt))
(setq org-latex-caption-above nil)
(setq org-html-validation-link nil)
(setq org-latex-compiler "lualatex")

(use-package ox-pandoc
  :ensure t)

(add-to-list 'org-latex-packages-alist '("" "listings" nil))
(setq org-latex-src-block-backend 'listings)
(setq org-latex-listings t)
(setq org-latex-listings-options '(("breaklines" "true")))

(defun reset-org-agenda-files (args)
  "Sometimes org uses some stale definition for the agenda files. With this function I reset them"
  (interactive "P")
  (setq org-agenda-files (directory-files-recursively org-directory "\\.org$")))

(reset-org-agenda-files nil)

(defun change-tag (old new)
  (when (member old (org-get-tags))
    (org-toggle-tag new 'on)
    (org-toggle-tag old 'off)
    ))

(defun org-rename-tag (old new)
  (interactive "scurrent tag: \nsnew name: ")
  (org-map-entries
   (lambda () (change-tag old new))
   (format "+%s" old)
   nil
   ))

(setq org-hide-emphasis-markers t)
(setq org-hide-leading-stars nil)
(setq org-src-fontify-natively t)
(setq org-src-content-indentation 0)
(setq org-fontify-quote-and-verse-blocks t)
(setq org-edit-src-content-indentation 0)
(setq org-image-actual-width '(700))
(setq org-pretty-entities t)
(setq org-auto-align-tags nil)

(require 'org-faces)

(require 'seq)
(defun setup-org-face-sizes ()
  "Increase the size of various headings and setup org-faces to fixed pitch"
  (interactive)
  ;; I don't want to do this if I'm running a solarized theme or zenburn
  (unless (or (member 'zenburn custom-enabled-themes)
              (seq-some (lambda (theme)
                          (string-prefix-p "solarized" (symbol-name theme)))
                        custom-enabled-themes))
    (dolist (face '((org-level-1 . 1.5)
                    (org-level-2 . 1.4)
                    (org-level-3 . 1.3)
                    (org-level-4 . 1.2)
                    (org-level-5 . 1.1)
                    (org-level-6 . 1.1)
                    (org-level-7 . 1.1)
                    (org-level-8 . 1.1)))
      (set-face-attribute (car face) nil :weight 'bold :height (cdr face))))
    ;; I do want the title to be another size
  (set-face-attribute 'org-document-title nil :weight 'bold :height 1.7)
  )
(add-hook 'org-mode-hook
          'setup-org-face-sizes)

(defun setup-org-faces ()
  "Ensure that anything that should be fixed-pitch and default in Org files appears that way"
  (set-face-attribute 'org-drawer nil :inherit 'default)
  (set-face-attribute 'org-block nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil  :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-block-begin-line nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-block-end-line nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-document-info-keyword nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face default))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
)

(add-hook 'org-mode-hook
          'setup-org-faces)

(use-package ob-restclient
  :ensure t)

(use-package ob-mermaid
  :ensure t)

(use-package ob-plantuml
  :config
  (setq org-plantuml-exec-mode 'plantuml))

(setq org-babel-python-command "python3")
(setq org-confirm-babel-evaluate nil)

(add-to-list 'org-src-lang-modes '("json" . json-ts))
(add-to-list 'org-src-lang-modes '("robot" . robot))

(defun setup-org-babel ()
  "Org babel languages"
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (scheme . t)
     (shell . t)
     (restclient . t)
     (mermaid . t)
     (plantuml . t)
     (eshell . t))))

(with-eval-after-load 'org (setup-org-babel))

(setq luamagick '(luamagick :programs ("lualatex" "convert")
                            :description "pdf > png"
                            :message "you need to install lualatex and imagemagick."
                            :use-xcolor t
                            :image-input-type "pdf"
                            :image-output-type "png"
                            :image-size-adjust (1.0 . 1.0)
                            :latex-compiler ("lualatex -interaction nonstopmode -output-directory %o %F")
                            :image-converter ("convert -density %D -trim -antialias %F -quality 100 %O")))
(add-to-list 'org-preview-latex-process-alist luamagick)
(setq org-preview-latex-default-process 'luamagick)


(use-package org-modern
  :ensure t
  :disabled
  :after org
  :hook (org-mode . #'org-modern))

(use-package visual-fill-column
  :ensure t
  :config
  (setq-default visual-fill-column-width 140
                visual-fill-column-center-text t))

(use-package org-present
  :ensure t
  :after org
  :config
  (add-hook 'org-present-mode-hook
            (lambda ()
              (org-display-inline-images)
              (org-present-read-only)))
  (add-hook 'org-present-mode-quit-hook
            (lambda ()
              (org-remove-inline-images)
              (org-present-read-write))))

(use-package olivetti
  :ensure t)

(use-package denote
  :ensure t
  :init
  (defun add-denote-file-to-org-agenda-files ()
    "Add the newly created Denote file to agenda files"
    (add-to-list 'org-agenda-files buffer-file-name))
  :config
  (setq denote-directory (expand-file-name org-directory))
  (setq denote-save-buffers nil)
  (setq denote-known-keywords '("emacs" "philosophy" "politics" "economics"))
  (setq denote-infer-keywords t)
  (setq denote-sort-keywords t)
  (setq denote-prompts '(title keywords))
  (setq denote-excluded-directories-regexp nil)
  (setq denote-excluded-keywords-regexp nil)
  (setq denote-rename-confirmations '(rewrite-front-matter modify-file-name))

  (require 'denote-journal-extras)
  (setq denote-journal-extras-title-format 'day-date-month-year)
  ;; Pick dates, where relevant, with Org's advanced interface:
  (setq denote-date-prompt-use-org-read-date t)

  (setq denote-backlinks-show-context t)

  (add-hook 'text-mode-hook #'denote-fontify-links-mode-maybe)

  ;; I want the new files added to agenda files immediately
  (add-hook 'denote-after-new-note-hook #'add-denote-file-to-org-agenda-files)

  (setq denote-dired-directories-include-subdirectories t)

  (add-hook 'dired-mode-hook #'denote-dired-mode-in-directories)

  (denote-rename-buffer-mode 1)

  (let ((map global-map))
    (define-key map (kbd "C-c n n") #'denote)
    (define-key map (kbd "C-c n J") #'denote-journal-extras-new-entry)
    (define-key map (kbd "C-c n j") #'denote-journal-extras-new-or-existing-entry)
    (define-key map (kbd "C-c n c") #'denote-region) ; "contents" mnemonic
    (define-key map (kbd "C-c n N") #'denote-type)
    (define-key map (kbd "C-c n d") #'denote-date)
    (define-key map (kbd "C-c n z") #'denote-signature) ; "zettelkasten" mnemonic
    (define-key map (kbd "C-c n s") #'denote-subdirectory)
    (define-key map (kbd "C-c n t") #'denote-template)
    ;; If you intend to use Denote with a variety of file types, it is
    ;; easier to bind the link-related commands to the `global-map', as
    ;; shown here.  Otherwise follow the same pattern for `org-mode-map',
    ;; `markdown-mode-map', and/or `text-mode-map'.
    (define-key map (kbd "C-c n l") #'denote-link-or-create)
    (define-key map (kbd "C-c n b") #'denote-backlinks)
    (define-key map (kbd "C-c n f f") #'denote-find-link)
    (define-key map (kbd "C-c n f b") #'denote-find-backlink)
    ;; Note that `denote-rename-file' can work from any context, not just
    ;; Dired bufffers.  That is why we bind it here to the `global-map'.
    (define-key map (kbd "C-c n r") #'denote-rename-file)
    (define-key map (kbd "C-c n R") #'denote-rename-file-using-front-matter))

  ;; Key bindings specifically for Dired.
  (let ((map dired-mode-map))
    (define-key map (kbd "C-c C-d C-i") #'denote-link-dired-marked-notes)
    (define-key map (kbd "C-c C-d C-r") #'denote-dired-rename-files)
    (define-key map (kbd "C-c C-d C-k") #'denote-dired-rename-marked-files-with-keywords)
    (define-key map (kbd "C-c C-d C-R") #'denote-dired-rename-marked-files-using-front-matter))

  (with-eval-after-load 'org-capture
    (setq denote-org-capture-specifiers "%l\n%i\n%?")
    (add-to-list 'org-capture-templates
                 '("n" "New note (with denote.el)" plain
                   (file denote-last-path)
                   #'denote-org-capture
                   :no-save t
                   :immediate-finish nil
                   :kill-buffer t
                   :jump-to-captured t)))

  ;; Also check the commands `denote-link-after-creating',
  ;; `denote-link-or-create'.  You may want to bind them to keys as well.

  (add-hook 'context-menu-functions #'denote-context-menu)
  )

(use-package consult-denote
  :ensure t
  :disabled ; currently donsn't work
  :config
  (consult-denote-mode 1))


(use-package citar
  :ensure t
  :no-require
  :custom
  (org-cite-global-bibliography `(,(concat (expand-file-name org-directory) "references.bib")))
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  (citar-bibliography org-cite-global-bibliography)
  ;; optional: org-cite-insert is also bound to C-c C-x C-@
  :bind
  (:map org-mode-map :package org ("C-c b" . #'org-cite-insert)))

(use-package citar-denote
  :ensure t
  :after (:any citar denote)
  :config
  (citar-denote-mode))

(setq bibtex-dialect 'biblatex)

;; fix apostrophes in flyspell
;; https://emacs.stackexchange.com/questions/58883/ispell-hunspell-recognizes-everything-after-the-apostrophe-a-different-word
(setq ispell-local-dictionary-alist
      '((nil "[[:alpha:]]" "[^[:alpha:]]" "['’]" t nil nil utf-8)))

(provide 'jh-writing)
