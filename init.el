;; -*- lexical-binding: t; -*-

(defvar site-lisp-folder
  (concat user-emacs-directory "site-elisp/"))
(add-to-list 'load-path site-lisp-folder)

(require 'init-main)

(require 'jh-writing)

(defun unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
	    ;; This would override `fill-column' if it's an integer.
	    (emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))

(keymap-global-set "M-Q" 'unfill-paragraph)

(defun copy-symbol-at-point ()
  "Copy the symbol at point"
  (interactive)
  (kill-new (thing-at-point 'symbol)))
